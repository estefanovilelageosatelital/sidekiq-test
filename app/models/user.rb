class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  after_create :send_welcome_email_to_user

  def send_welcome_email_to_user
    UserMailer.welcome_email(self).deliver_later

    # ScheduleEmailDeliveryJob.set(wait_until: Date.tomorrow.noon).perform_later(self)
    # ScheduleEmailDeliveryJob.set(wait: 1.week).perform_later(self)
    # ScheduleEmailDeliveryJob.perform_later(self)
  end
end

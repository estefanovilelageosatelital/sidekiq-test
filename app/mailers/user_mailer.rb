class UserMailer < ApplicationMailer
    default from: 'estefanovilelazarate@gmail.com'

    def welcome_email(user)
        @user = user
        # @url  = 'http://example.com/login'
        mail(to: user.email, subject: 'Bienvenido!')

        # ScheduleEmailDeliveryJob.perform_later(args)
    end
end